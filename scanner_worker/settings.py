import dramatiq
from dramatiq.brokers.rabbitmq import RabbitmqBroker
import sentry_dramatiq
import sentry_sdk
import yaml
from periodiq import PeriodiqMiddleware

# with open("/home/timur-pc/PycharmProjects/workerSubscriptionScanner/settings.yml") as f:
with open("/etc/opt/workerSubscriptionScanner/settings.yml") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

sentry_sdk.init(
    **config["sentry"],
    integrations=[sentry_dramatiq.DramatiqIntegration()],
)

dramatiq.set_broker(RabbitmqBroker(url=config["rmq"]["url"]))
dramatiq.get_broker().add_middleware(PeriodiqMiddleware())
