from datetime import datetime

import dramatiq
import pytz
from periodiq import cron
from pygeos import Geometry, area, intersection, to_wkt, union

from scanner_worker.settings import config
from scanner_worker.utils import (DataBaseUtil, EmailUtil, SearchCatalogApi,
                                  resolution_mapping)


@dramatiq.actor(
    max_retries=1, queue_name=config["rmq"]["queue"], periodic=cron("* * * * *")
)
def search():
    check_date = str(datetime.isoformat(datetime.now(pytz.timezone("Europe/Moscow"))))
    db_con = DataBaseUtil(**config["database"])
    try:
        email_util = EmailUtil(**config["smtp"])
        try:
            search_catalog_util = SearchCatalogApi(
                config["search_catalog_url"], config["keycloak"]
            )
            notics = db_con.get_cooking_notics()
            for notic in notics:
                start_search_at = str(datetime.isoformat(notic["start_search_at"]))
                images = search_catalog_util.get_images(
                    notic["roi"], start_search_at, check_date
                )
                if not images:
                    continue
                suitable_images = []
                roi_geometry = Geometry(notic["roi"])
                min_resolution, max_resolution = resolution_mapping(notic["resolution"])
                full_intersection_geometry = None
                for image in images:
                    if (
                        notic["max_cloudiness"] < image["cloudiness"]
                        or not (min_resolution <= image["resolution"] < max_resolution)
                        or image["pole_area_intersection"]
                    ):
                        continue
                    suitable_images.append(image["identifier"])
                    intersection_geometry = intersection(
                        roi_geometry, Geometry(image["geometry"])
                    )
                    print(image["geometry"])
                    print(
                        "Координаты пересечения",
                        to_wkt(intersection_geometry, rounding_precision=-1),
                    )
                    if full_intersection_geometry:
                        full_intersection_geometry = union(
                            full_intersection_geometry, intersection_geometry
                        )
                    else:
                        full_intersection_geometry = intersection_geometry
                if full_intersection_geometry:
                    print(to_wkt(full_intersection_geometry, rounding_precision=-1))
                    percent = (
                        area(full_intersection_geometry) / area(roi_geometry) * 100
                    )
                    print(f"Процент пересечения - {percent}")
                    if (
                        area(full_intersection_geometry) / area(roi_geometry) * 100
                        >= notic["min_intersection"]
                    ):
                        db_con.update_notic_to_send(
                            notic["id"], notic["sub_id"], check_date
                        )
                        email_util.send_mail(
                            notic["user_email"], notic["title"], suitable_images
                        )
                        db_con.connection.commit()
        finally:
            email_util.quit()
    finally:
        db_con.close()
