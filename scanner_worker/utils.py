import smtplib
from email.message import EmailMessage

import psycopg2
import requests


class SearchCatalogApi:
    def __init__(self, url, auth):
        self.url = url
        self.auth = auth

    def get_token(self):
        try:
            response = requests.post(
                self.auth["get_token_url"],
                data={
                    "client_id": self.auth["client_id"],
                    "client_secret": self.auth["client_secret"],
                    "grant_type": self.auth["grant_type"],
                },
            )
            if response.status_code == 200:
                return response.json()["access_token"]
            raise Exception('Ошибка при получении токена')
        except (requests.Timeout, requests.ConnectionError):
            raise Exception('Ошибка при получении токена')

    def get_images(self, roi, date_after, date_before):
        headers = {"Authorization": f"Bearer {self.get_token()}"}
        full_url = (
            self.url
            + "?"
            + "geometry="
            + roi
            + "&acquisition_date_after="
            + date_after
            + "&acquisition_date_before="
            + date_before
        )
        try:
            response = requests.get(full_url, headers=headers)
            if response.status_code == 200:
                return response.json()["results"]
            raise Exception('Ошибка при обращении к WebSearchCatalog')
        except (requests.Timeout, requests.ConnectionError):
            raise Exception('Ошибка при обращении к WebSearchCatalog')


class EmailUtil:
    def __init__(self, host, port, from_addr, password):
        self.from_addr = from_addr
        self.password = password
        self.connection = smtplib.SMTP(host, port, timeout=30)
        self.connection.login(from_addr, password)

    def send_mail(self, to_addr, sub_title, images):
        message = EmailMessage()
        message["Subject"] = "По вашему району интереса появились новые снимки"
        message["From"] = self.from_addr
        message["To"] = to_addr
        images_html = []
        for image in images:
            images_html.append(f"<li>{image}</li>")
        text = "<ul>" + "".join(images_html) + "</ul>"
        message.set_content(
            f"<div>По вашему району интереса {sub_title} появились снимки: {text}</div>", subtype="html",
        )
        self.connection.send_message(message)

    def quit(self):
        self.connection.quit()


class DataBaseUtil:
    def __init__(self, dbname, schema, user, password, host):
        self.connection = psycopg2.connect(
            dbname=dbname,
            host=host,
            user=user,
            password=password,
            options=f"-c search_path={schema}",
        )
        self.cursor = self.connection.cursor()

    def get_cooking_notics(self):
        self.cursor.execute(
            "SELECT N.id, N.sub_id, S.max_cloudiness, S.min_intersection, S.resolution, "
            "S.roi, N.images, S.start_search_at, S.user_email, S.title "
            'FROM "subscription".notifications N JOIN "subscription".subs S ON N.sub_id=S.id '
            "WHERE N.status = 'cooking' FOR UPDATE;"
        )
        return self.get_result_as_dict()

    def get_result_as_dict(self):
        a = [
            dict(zip([column[0] for column in self.cursor.description], row))
            for row in self.cursor.fetchall()
        ]
        return a

    def update_notic_to_send(self, notic_id, sub_id, start_search_at):
        self.cursor.execute(
            "UPDATE notifications " "SET status='sent' " f"WHERE id={notic_id}"
        )
        self.cursor.execute(
            "INSERT INTO notifications (sub_id, status) "
            f"VALUES ({sub_id}, 'cooking');"
        )
        self.cursor.execute(
            f"UPDATE subs SET start_search_at='{start_search_at}' WHERE id={sub_id}"
        )

    def close(self):
        if self.cursor is not None:
            self.cursor.close()
        if self.connection is not None:
            self.connection.close()


def resolution_mapping(resolution):
    mapping = {
        "ultra_high": (0, 1),
        "high": (1, 10),
        "medium": (10, 100),
        "low": (100, 1000),
    }
    return min(mapping[resolution]), max(mapping[resolution])
