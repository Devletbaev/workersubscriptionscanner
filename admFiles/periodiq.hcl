template {
  contents = "{{ $keys_path := printf \"%s/%s/settings.yml\" (env \"ENVIRONMENT\") (env \"APPID\") -}}{{ key $keys_path }}"
  destination = "/etc/opt/workerSubscriptionScanner/settings.yml"
  create_dest_dirs = true
  perms = 0644
}

exec {
  command = "periodiq scanner_worker.tasks"
  reload_signal = "SIGHUP"
}
