from io import open
from os import path

from setuptools import find_packages, setup

with open(path.join("README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="workerSubscriptionScanner",
    version="0.1.0",
    description=long_description,
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=find_packages(),
    python_requires=">=3",
    install_requires=[
        "dramatiq[rabbitmq]==1.11.0",
        "periodiq==0.12.1",
        "psycopg2==2.9.1",
        "pygeos==0.10",
        "python-keycloak==0.25.0",
        "PyYAML==5.4.1",
        "requests==2.25.1",
        "sentry-dramatiq ==0.3.2",
        "pytz==2021.1",
    ],
)
