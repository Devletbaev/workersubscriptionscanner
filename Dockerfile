FROM gl-registry.ext-dc1.o.nkpor.etris/adm/images/centosbaseweb/centos_base_web:latest
#
# Consul settings
ENV ENVIRONMENT=development \
    APPID=worker-subscription-scanner \
    HOST_IP="127.0.0.1" \
    PYTHONUNBUFFERED=1
#
COPY admFiles/*.hcl /etc/consul/
COPY admFiles/*.repo /etc/yum.repos.d/
COPY admFiles/env.sh /tmp/
COPY dist /tmp/
#
RUN set -xe ; \
    source /tmp/env.sh ;\
    yum makecache ;\
    yum -y install qt-postgresql postgresql-devel curl ;\
    if [[ -f "/tmp/${PACKAGE_NAME}-${PACKAGE_VERSION}.tar.gz" ]] ;\
       then pip3 install /tmp/${PACKAGE_NAME}-${PACKAGE_VERSION}.tar.gz ;\
       else pip3 install $PACKAGE_NAME==$PACKAGE_VERSION ;\
       fi ;\
    yum clean all ;\
    rm -rf /var/cache/yum/ ;\
#    
# consul settings
    SHARE="http://nexus.ext-dc1.o.nkpor.etris:8081/repository/share/consul" ;\
    FILENAME="consul-template_0.25.0_linux_amd64.tgz" ;\
    curl --silent --output ${FILENAME} ${SHARE}/${FILENAME} ;\
    tar xvzf ${FILENAME} -C /usr/bin/ ;\
    rm -f ${FILENAME} ;\
#
    python  --version ;\
    python3 --version ;\
    pip3    --version ;\
    pip3 list ;\
    date
#
ENTRYPOINT /usr/bin/consul-template -consul-addr "${HOST_IP}:8500" -config /etc/consul/dramatiq.hcl
#
